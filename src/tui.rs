use pancurses::{curs_set, endwin, initscr, noecho, Input, Window};

use crate::options::{CliOption, CliType, TuiPage};
use crate::{Argument, Arguments, CliOptions};

const MAX_TAB: i32 = 5;
const VALUE_FIELD_LENGTH: i32 = 15;

pub fn start(all_options: &CliOptions) -> Option<Arguments> {
    let window = initscr();
    let mut selected_tab: i32 = 0;
    let mut scroll_position: i32 = 0;
    noecho();
    curs_set(0);
    window.keypad(true);
    let help = Help::new();

    loop {
        tab_bar(&window, selected_tab);
        main_content(&window, all_options, selected_tab, &mut scroll_position);
        help.draw_bar(&window);
        match window.getch().unwrap() {
            /*
                Cancel doesn't need to be there because it is handled by ^C
            */
            Input::Character('?') => {
                help.draw_window(&window);
            }
            Input::KeyLeft => {
                tab_left(&mut selected_tab);
            }
            Input::Character('h') => {
                tab_left(&mut selected_tab);
            }
            Input::KeyRight => {
                tab_right(&mut selected_tab);
            }
            Input::Character('l') => {
                tab_right(&mut selected_tab);
            }
            Input::KeyDown => {
                scroll_position += 1;
            }
            Input::Character('j') => {
                scroll_position += 1;
            }
            Input::KeyUp => {
                scroll_position -= 1;
            }
            Input::Character('k') => {
                scroll_position -= 1;
            }
            Input::Character('r') => {
                break;
            }
            Input::KeyEnter => {
                // Select option
            }
            Input::KeyResize => {
                continue; //to redraw the window
            }
            _ => {}
        }
    }
    endwin();
    return Some(vec![Argument {
        value: String::from("-h"),
        option: all_options
            .iter()
            .find(|op| op.name == "-h")
            .unwrap()
            .to_owned(),
    }]);
}

fn tab_right(selected_tab: &mut i32) {
    *selected_tab += 1;
    *selected_tab %= MAX_TAB;
}

fn tab_left(selected_tab: &mut i32) {
    *selected_tab -= 1;
    *selected_tab %= MAX_TAB;
    if *selected_tab < 0 {
        *selected_tab += MAX_TAB;
    }
}

fn add_string_field(lines: &mut Vec<String>, value: String) {
    let needed_length = VALUE_FIELD_LENGTH as usize - 2;
    let mut inner: String = String::from(value.to_owned())
        .drain(0..needed_length.min(value.len()))
        .collect();
    if inner.len() < needed_length {
        inner += &line_of_chars(" ", (needed_length - inner.len()) as i32);
    }

    let up____ = "╭─────────────╮";
    let midd = format!("│{inner}│",);
    let down__ = "╰─────────────╯";

    lines[0] += up____;
    lines[1] += &midd;
    lines[2] += down__;
}

fn add_bool_field(lines: &mut Vec<String>, value: String) {
    let mut inner = "🮱";
    if value.is_empty() {
        inner = "🯀"
    }

    let up____ = "           ╭──╮";
    let midd = format!("           │{inner} │",);
    let down__ = "           ╰──╯";

    lines[0] += up____;
    lines[1] += &midd;
    lines[2] += down__;
}

fn add_value_field(lines: &mut Vec<String>, value_type: &CliType, value: String) {
    match *value_type {
        CliType::Boolean => add_bool_field(lines, value),
        _ => add_string_field(lines, value),
    }
}

fn calculate_margin(window: &Window) -> (i32, i32) {
    let x = window.get_max_x() / 5;
    (x, x * 3)
}

fn format_option(option: &CliOption, length: i32) -> Vec<String> {
    let mut lines = Vec::new();

    let mut name_line = option.name.to_owned();
    let missing_length = length - name_line.len() as i32 - VALUE_FIELD_LENGTH;
    name_line += &line_of_chars(" ", missing_length);

    lines.push(line_of_chars(" ", length - VALUE_FIELD_LENGTH));
    lines.push(name_line);
    lines.push(line_of_chars(" ", length - VALUE_FIELD_LENGTH));
    lines.push(option.help_short.to_owned());

    add_value_field(&mut lines, &option.type_accepted, String::from("TODO"));

    lines
}

fn find_selected_line(option_positions: &Vec<i32>, scroll_position: &i32) -> i32 {
    let mut selected_line = 0;
    for (i, position) in option_positions.iter().enumerate() {
        if *scroll_position > *position {
            if i + 1 < option_positions.len() {
                selected_line = option_positions[i + 1];
            } else {
                selected_line = option_positions[i];
            }
        }
    }
    selected_line
}

fn limit_scroll_position(scroll_position: &mut i32, max_scroll: i32) {
    match *scroll_position {
        p if p < 0 => *scroll_position = 0,
        p if p > max_scroll => *scroll_position = max_scroll,
        _ => (),
    }
}

fn generate_content_lines(options: &CliOptions, content_len: i32) -> (Vec<String>, Vec<i32>) {
    let mut lines: Vec<String> = Vec::new();
    let mut option_positions = Vec::new();

    for option in options {
        option_positions.push(lines.len() as i32);
        lines.append(&mut format_option(&option, content_len));
        lines.push(String::new());
    }

    (lines, option_positions)
}

fn print_scroll_indicator(window: &Window, scroll_position: i32, max_scroll: i32) {
    window.mv(window.get_max_y() - 3, 0);
    window.printw(format!("{}/{}", scroll_position, max_scroll));
}

fn main_content(
    window: &Window,
    all_options: &CliOptions,
    selected_tab: i32,
    scroll_position: &mut i32,
) {
    let (margin, content_len) = calculate_margin(window);
    let (mut lines, option_positions) = generate_content_lines(all_options, content_len);
    let mut selected_line = find_selected_line(&option_positions, scroll_position);

    limit_scroll_position(scroll_position, *option_positions.last().unwrap());

    for i in 2..window.get_max_y() - 2 {
        window.mv(i, 0);
        window.clrtoeol();
    }

    print_scroll_indicator(window, *scroll_position, *option_positions.last().unwrap());

    lines.drain(0..*scroll_position as usize);
    selected_line -= *scroll_position;

    for (i, line) in lines.iter().enumerate() {
        if i as i32 == selected_line {
            window.mv(3 + i as i32, margin - 6);
            window.printw(">>>");
        }
        window.mv(2 + i as i32, margin);
        window.printw(line);
    }
}

struct Help {
    basic: Vec<&'static str>,
    extended: Vec<&'static str>,
}

impl Help {
    fn new() -> Help {
        let basic = vec![
            "Enter = Select",
            "^C = quit",
            "? = more help",
            "r = execute",
        ];
        let mut extended = basic.to_owned();
        extended.append(&mut vec![
            "up/down/j/k = scroll",
            "1/2/3/4/5/6/7/8/9 = set profile number N",
            "left/right/h/l = switch tabs",
        ]);

        Help { basic, extended }
    }

    fn draw_bar(&self, window: &Window) {
        let height = window.get_max_y() - 1;
        let width = window.get_max_x();
        let separator = line_of_chars("▁", width);

        window.mv(height - 1, 0);
        window.printw(separator);
        window.mv(height, 0);
        window.clrtoeol();

        for command in self.basic.iter() {
            window.printw(command);
            window.printw(" | ");
        }
    }

    fn draw_window(&self, window: &Window) {
        let max_x = window.get_max_x();
        let max_y = window.get_max_y();

        let subwin = window
            .subwin(max_y / 2, max_x / 2, max_y / 4, max_x / 4)
            .unwrap();
        draw_border(&subwin);

        for (i, line) in self.extended.iter().enumerate() {
            subwin.mv(i as i32 + 1, 1);
            subwin.printw(line);
        }

        loop {
            if subwin.getch() == Some(Input::Character('?')) {
                break;
            }
        }

        subwin.delwin();
        window.touch();
    }
}

fn tab_bar(window: &Window, selected_tab: i32) {
    let pages = get_page_list();
    let max_x = window.get_max_x();
    let page_len = max_x / pages.len() as i32;
    let separator = line_of_chars("▔", max_x);

    window.mv(0, 0);

    for (i, page) in pages.iter().enumerate() {
        window.printw(&create_tab(page, page_len, selected_tab == i as i32));
    }

    window.mv(1, 0);
    window.printw(separator);
}

fn create_tab(title: &str, len: i32, selected: bool) -> String {
    let mut tab = String::from("|");

    let mut spacing_character = " ";
    let spacing_total_len = len - 1 - title.len() as i32;
    let mut r_spacing_len = 0;
    let mut l_spacing_len = 0;

    if spacing_total_len > 0 {
        // Stretch spacing to fill window length
        while r_spacing_len + l_spacing_len < spacing_total_len {
            r_spacing_len += 1;
            l_spacing_len += 1;
        }
        if spacing_total_len != r_spacing_len + l_spacing_len {
            r_spacing_len -= 1;
        }
    }

    if selected {
        spacing_character = "░";
    }

    let l_spacing = line_of_chars(spacing_character, l_spacing_len);
    let r_spacing = line_of_chars(spacing_character, r_spacing_len);

    tab += &l_spacing;
    tab += title;
    tab += &r_spacing;
    tab
}

fn get_page_list() -> Vec<String> {
    use TuiPage::{Audio, General, Misc, Video};
    let mut pages: Vec<String> = vec![General, Video, Audio, Misc]
        .iter()
        .map(|p| format!("{:?}", p))
        .collect();
    pages.push(String::from("History"));
    pages
}

fn line_of_chars(ch: &str, len: i32) -> String {
    let mut line = String::new();
    for _ in 0..len {
        line += ch;
    }
    line
}

fn draw_border(window: &Window) {
    let x = window.get_max_x() - 1;
    let y = window.get_max_y() - 1;

    let horizontal = line_of_chars("─", x);
    window.mv(0, 0);
    window.printw(horizontal.to_owned());
    window.mv(y, 0);
    window.printw(horizontal);

    for i in 0..window.get_max_y() {
        window.mv(i, 0);
        window.printw("│");
        window.mv(i, x);
        window.printw("│");
    }

    window.mv(0, 0);
    window.printw("╭");
    window.mv(0, x);
    window.printw("╮");
    window.mv(y, 0);
    window.printw("╰");
    window.mv(y, x);
    window.printw("╯");
}
