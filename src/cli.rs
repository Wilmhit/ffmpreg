use crate::{
    options::{CliOption, CliType},
    Argument, Arguments, CliOptions,
};
use std::env::args;

pub fn collect(all_options: &CliOptions) -> Option<Arguments> {
    let mut args: Vec<_> = args().collect();
    args.drain(0..1);

    let valid_args = parse_arguments(args, all_options);

    {
        if valid_args.len() != 0 {
            Some(valid_args)
        } else {
            None
        }
    }
}

fn parse_arguments(cli_input: Vec<String>, possible_options: &CliOptions) -> Arguments {
    let mut parsed = Vec::new();

    /*
    This loop must skip some arguments in case a particular option takes
    in a value (and spans across 2 arguments). That's why there is offset.
    */
    let mut offset = 0;
    for arg_num in 0..cli_input.len() {
        let used_option = cli_input.get(arg_num + offset).unwrap();
        let next_arg = cli_input.get(arg_num + offset);
        let (argument, is_double) = parse_arg(used_option, next_arg, possible_options);

        parsed.push(argument);

        if is_double {
            offset += 1;
        }
    }
    return parsed;
}

fn parse_arg(
    this_arg: &String,
    next_arg: Option<&String>,
    possible_options: &CliOptions,
) -> (Argument, bool) {
    match possible_options.iter().find(|opt| opt.name == *this_arg) {
        Some(opt) => return fill_argument(next_arg, opt),
        None => {
            // Option is not known. Create a temp option.
            return (
                Argument {
                    value: this_arg.to_owned(),
                    option: CliOption::from_name_only(this_arg.to_owned()),
                },
                false,
            );
        }
    }
}

fn fill_argument(next_arg: Option<&String>, opt: &CliOption) -> (Argument, bool) {
    let is_double = !matches!(opt.type_accepted, CliType::Boolean);
    let arg = Argument {
        option: opt.to_owned(),
        value: {
            if is_double {
                next_arg.unwrap().to_owned()
            } else {
                opt.name.to_owned()
            }
        },
    };
    return (arg, is_double);
}
