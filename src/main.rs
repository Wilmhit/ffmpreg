use std::process::Command;

use options::{get_options, CliOption};

mod cli;
mod options;
mod tui;

const EXECUTABLE: &str = "/usr/bin/ffmpeg";
const EXECUTABLE_NOT_FOUND_EXIT_CODE: i32 = 127;

#[derive(Debug)]
pub struct Argument {
    option: CliOption,
    value: String,
}

type CliOptions = Vec<CliOption>;
type Arguments = Vec<Argument>;

fn main() {
    better_panic::install();
    let possible_options = get_options();

    //First try CLI, then try TUI, then print help
    if let Some(commands) = cli::collect(&possible_options) {
        execute(commands);
        return;
    };
    if let Some(commands) = tui::start(&possible_options) {
        execute(commands);
        return;
    };
    print_help();
}

fn execute(arguments: Arguments) -> i32 {
    let argument_strings = arguments.iter().map(|arg| &arg.value);
    let mut command = Command::new(EXECUTABLE);
    command.args(argument_strings);

    println!("Executing: [{:?}]", command); //TODO better printing

    if let Ok(mut child) = command.spawn() {
        child
            .wait()
            .expect("ffmpeg child process has failed before run")
            .code()
            .expect("ffmpeg child process terminated with signal")
    } else {
        println!(concat!(
            "Could not create ffmpeg child process. ",
            "Is the executable correct?"
        ));
        EXECUTABLE_NOT_FOUND_EXIT_CODE
    }
}

fn print_help() {
    println!(concat!(
        "Could not execute this command. ",
        "Please either enter TUI or add command line options."
    ))
}
