use crate::CliOptions;
use serde_json;
use std::str::FromStr;
use strum_macros::EnumString;

const PARS_ERR: &str = "Error while internally parsing possible options";
const OPTIONS_JSON_STRING: &str = include_str!("../options.json");

#[derive(Debug, Clone, EnumString)]
pub enum CliType {
    Boolean,
    String,
    PostiveNumber,
    IntegerNumber,
    RealNumber,
}

#[derive(Debug, Clone, EnumString)]
pub enum TuiPage {
    General,
    Audio,
    Video,
    Misc,
    Hidden,
}

#[derive(Debug, Clone)]
pub struct CliOption {
    pub name: String,
    pub type_accepted: CliType,
    pub help_short: String,
    pub help_long: String,
    pub page: TuiPage,
}

impl CliOption {
    fn from_json(j: &serde_json::Value) -> Option<CliOption> {
        Some(CliOption {
            name: j["name"].as_str()?.to_string(),
            type_accepted: CliType::from_str(j["type_accepted"].as_str()?).unwrap(),
            help_short: j["help_short"].as_str()?.to_string(),
            help_long: j["help_long"].as_str()?.to_string(),
            page: TuiPage::from_str(j["page"].as_str()?).unwrap(),
        })
    }

    pub fn from_name_only(name: String) -> CliOption {
        CliOption {
            name,
            type_accepted: CliType::Boolean,
            help_short: "".to_string(),
            help_long: "".to_string(),
            page: TuiPage::Hidden,
        }
    }
}

fn parse_options(json: &serde_json::Value) -> CliOptions {
    json.as_array()
        .expect(PARS_ERR)
        .iter()
        .map(|opt| CliOption::from_json(opt).expect(PARS_ERR))
        .collect()
}

pub fn get_options() -> CliOptions {
    let json = serde_json::from_str(OPTIONS_JSON_STRING).expect(PARS_ERR);
    return parse_options(&json);
}
